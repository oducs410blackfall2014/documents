
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 11/03/2014 14:23:52
-- Generated from EDMX file: C:\Users\mhines\Documents\Visual Studio 2013\Projects\Test\Test\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [test_sql];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_UserProjectMember]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ProjectMemberships] DROP CONSTRAINT [FK_UserProjectMember];
GO
IF OBJECT_ID(N'[dbo].[FK_ProjectMemberProject]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ProjectMemberships] DROP CONSTRAINT [FK_ProjectMemberProject];
GO
IF OBJECT_ID(N'[dbo].[FK_ProjectRolesProjectMembership]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ProjectRoles] DROP CONSTRAINT [FK_ProjectRolesProjectMembership];
GO
IF OBJECT_ID(N'[dbo].[FK_ProjectSyllabary]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Syllabaries] DROP CONSTRAINT [FK_ProjectSyllabary];
GO
IF OBJECT_ID(N'[dbo].[FK_SyllabaryColumnSyllabary]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SyllabaryColumnHeaders] DROP CONSTRAINT [FK_SyllabaryColumnSyllabary];
GO
IF OBJECT_ID(N'[dbo].[FK_SyllabaryRowSyllabary]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SyllabaryRowHeaders] DROP CONSTRAINT [FK_SyllabaryRowSyllabary];
GO
IF OBJECT_ID(N'[dbo].[FK_SyllabaryCellSyllabary]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SyllabaryCells] DROP CONSTRAINT [FK_SyllabaryCellSyllabary];
GO
IF OBJECT_ID(N'[dbo].[FK_SyllabaryCellSyllabaryRowHeader]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SyllabaryCells] DROP CONSTRAINT [FK_SyllabaryCellSyllabaryRowHeader];
GO
IF OBJECT_ID(N'[dbo].[FK_SyllabaryCellSyllabaryColumnHeader]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SyllabaryCells] DROP CONSTRAINT [FK_SyllabaryCellSyllabaryColumnHeader];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Syllabaries]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Syllabaries];
GO
IF OBJECT_ID(N'[dbo].[Projects]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Projects];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO
IF OBJECT_ID(N'[dbo].[ProjectRoles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ProjectRoles];
GO
IF OBJECT_ID(N'[dbo].[ProjectMemberships]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ProjectMemberships];
GO
IF OBJECT_ID(N'[dbo].[SyllabaryColumnHeaders]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SyllabaryColumnHeaders];
GO
IF OBJECT_ID(N'[dbo].[SyllabaryRowHeaders]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SyllabaryRowHeaders];
GO
IF OBJECT_ID(N'[dbo].[SyllabaryCells]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SyllabaryCells];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Syllabaries'
CREATE TABLE [dbo].[Syllabaries] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Project_Id] int  NOT NULL
);
GO

-- Creating table 'Projects'
CREATE TABLE [dbo].[Projects] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [CreationTime] time  NOT NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [UserName] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Password] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ProjectRoles'
CREATE TABLE [dbo].[ProjectRoles] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [ProjectRolesProjectMembership_ProjectRoles_Id] int  NOT NULL
);
GO

-- Creating table 'ProjectMemberships'
CREATE TABLE [dbo].[ProjectMemberships] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [User_Id] int  NOT NULL,
    [Projects_Id] int  NOT NULL
);
GO

-- Creating table 'SyllabaryColumnHeaders'
CREATE TABLE [dbo].[SyllabaryColumnHeaders] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Pronunciation] nvarchar(max)  NOT NULL,
    [IPA] nvarchar(max)  NOT NULL,
    [Index] nvarchar(max)  NOT NULL,
    [Syllabary_Id] int  NOT NULL
);
GO

-- Creating table 'SyllabaryRowHeaders'
CREATE TABLE [dbo].[SyllabaryRowHeaders] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Pronunciation] nvarchar(max)  NOT NULL,
    [IPA] nvarchar(max)  NOT NULL,
    [Index] nvarchar(max)  NOT NULL,
    [Syllabary_Id] int  NOT NULL
);
GO

-- Creating table 'SyllabaryCells'
CREATE TABLE [dbo].[SyllabaryCells] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Syllabary_Id] int  NOT NULL,
    [Row_Id] int  NOT NULL,
    [Column_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Syllabaries'
ALTER TABLE [dbo].[Syllabaries]
ADD CONSTRAINT [PK_Syllabaries]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Projects'
ALTER TABLE [dbo].[Projects]
ADD CONSTRAINT [PK_Projects]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ProjectRoles'
ALTER TABLE [dbo].[ProjectRoles]
ADD CONSTRAINT [PK_ProjectRoles]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ProjectMemberships'
ALTER TABLE [dbo].[ProjectMemberships]
ADD CONSTRAINT [PK_ProjectMemberships]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SyllabaryColumnHeaders'
ALTER TABLE [dbo].[SyllabaryColumnHeaders]
ADD CONSTRAINT [PK_SyllabaryColumnHeaders]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SyllabaryRowHeaders'
ALTER TABLE [dbo].[SyllabaryRowHeaders]
ADD CONSTRAINT [PK_SyllabaryRowHeaders]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SyllabaryCells'
ALTER TABLE [dbo].[SyllabaryCells]
ADD CONSTRAINT [PK_SyllabaryCells]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [User_Id] in table 'ProjectMemberships'
ALTER TABLE [dbo].[ProjectMemberships]
ADD CONSTRAINT [FK_UserProjectMember]
    FOREIGN KEY ([User_Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UserProjectMember'
CREATE INDEX [IX_FK_UserProjectMember]
ON [dbo].[ProjectMemberships]
    ([User_Id]);
GO

-- Creating foreign key on [Projects_Id] in table 'ProjectMemberships'
ALTER TABLE [dbo].[ProjectMemberships]
ADD CONSTRAINT [FK_ProjectMemberProject]
    FOREIGN KEY ([Projects_Id])
    REFERENCES [dbo].[Projects]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ProjectMemberProject'
CREATE INDEX [IX_FK_ProjectMemberProject]
ON [dbo].[ProjectMemberships]
    ([Projects_Id]);
GO

-- Creating foreign key on [ProjectRolesProjectMembership_ProjectRoles_Id] in table 'ProjectRoles'
ALTER TABLE [dbo].[ProjectRoles]
ADD CONSTRAINT [FK_ProjectRolesProjectMembership]
    FOREIGN KEY ([ProjectRolesProjectMembership_ProjectRoles_Id])
    REFERENCES [dbo].[ProjectMemberships]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ProjectRolesProjectMembership'
CREATE INDEX [IX_FK_ProjectRolesProjectMembership]
ON [dbo].[ProjectRoles]
    ([ProjectRolesProjectMembership_ProjectRoles_Id]);
GO

-- Creating foreign key on [Project_Id] in table 'Syllabaries'
ALTER TABLE [dbo].[Syllabaries]
ADD CONSTRAINT [FK_ProjectSyllabary]
    FOREIGN KEY ([Project_Id])
    REFERENCES [dbo].[Projects]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ProjectSyllabary'
CREATE INDEX [IX_FK_ProjectSyllabary]
ON [dbo].[Syllabaries]
    ([Project_Id]);
GO

-- Creating foreign key on [Syllabary_Id] in table 'SyllabaryColumnHeaders'
ALTER TABLE [dbo].[SyllabaryColumnHeaders]
ADD CONSTRAINT [FK_SyllabaryColumnSyllabary]
    FOREIGN KEY ([Syllabary_Id])
    REFERENCES [dbo].[Syllabaries]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SyllabaryColumnSyllabary'
CREATE INDEX [IX_FK_SyllabaryColumnSyllabary]
ON [dbo].[SyllabaryColumnHeaders]
    ([Syllabary_Id]);
GO

-- Creating foreign key on [Syllabary_Id] in table 'SyllabaryRowHeaders'
ALTER TABLE [dbo].[SyllabaryRowHeaders]
ADD CONSTRAINT [FK_SyllabaryRowSyllabary]
    FOREIGN KEY ([Syllabary_Id])
    REFERENCES [dbo].[Syllabaries]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SyllabaryRowSyllabary'
CREATE INDEX [IX_FK_SyllabaryRowSyllabary]
ON [dbo].[SyllabaryRowHeaders]
    ([Syllabary_Id]);
GO

-- Creating foreign key on [Syllabary_Id] in table 'SyllabaryCells'
ALTER TABLE [dbo].[SyllabaryCells]
ADD CONSTRAINT [FK_SyllabaryCellSyllabary]
    FOREIGN KEY ([Syllabary_Id])
    REFERENCES [dbo].[Syllabaries]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SyllabaryCellSyllabary'
CREATE INDEX [IX_FK_SyllabaryCellSyllabary]
ON [dbo].[SyllabaryCells]
    ([Syllabary_Id]);
GO

-- Creating foreign key on [Row_Id] in table 'SyllabaryCells'
ALTER TABLE [dbo].[SyllabaryCells]
ADD CONSTRAINT [FK_SyllabaryCellSyllabaryRowHeader]
    FOREIGN KEY ([Row_Id])
    REFERENCES [dbo].[SyllabaryRowHeaders]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SyllabaryCellSyllabaryRowHeader'
CREATE INDEX [IX_FK_SyllabaryCellSyllabaryRowHeader]
ON [dbo].[SyllabaryCells]
    ([Row_Id]);
GO

-- Creating foreign key on [Column_Id] in table 'SyllabaryCells'
ALTER TABLE [dbo].[SyllabaryCells]
ADD CONSTRAINT [FK_SyllabaryCellSyllabaryColumnHeader]
    FOREIGN KEY ([Column_Id])
    REFERENCES [dbo].[SyllabaryColumnHeaders]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SyllabaryCellSyllabaryColumnHeader'
CREATE INDEX [IX_FK_SyllabaryCellSyllabaryColumnHeader]
ON [dbo].[SyllabaryCells]
    ([Column_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------