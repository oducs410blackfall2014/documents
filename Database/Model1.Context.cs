﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Test
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class Model1Container : DbContext
    {
        public Model1Container()
            : base("name=Model1Container")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Syllabary> Syllabaries { get; set; }
        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<ProjectRoles> ProjectRoles { get; set; }
        public virtual DbSet<ProjectMembership> ProjectMemberships { get; set; }
        public virtual DbSet<SyllabaryColumnHeader> SyllabaryColumnHeaders { get; set; }
        public virtual DbSet<SyllabaryRowHeader> SyllabaryRowHeaders { get; set; }
        public virtual DbSet<SyllabaryCell> SyllabaryCells { get; set; }
    }
}
